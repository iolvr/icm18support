package icmua.parseweathersample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        callRemoteWheatherAndCreateSimpleWeatherEntryObjects();

    }

    private void callRemoteWheatherAndCreateSimpleWeatherEntryObjects() {
        String url = "https://samples.openweathermap.org/data/2.5/forecast/daily?id=524901&lang=zh_cn&appid=b1b15e88fa797225412429c1c50c122a1";
        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(LOG_TAG, "got response");

                        // Parse the JSON into a list of weather forecasts
                        try {
                            WeatherResponse parsedResponse = new OpenWeatherJsonParser().parse(response.toString());
                            Log.d(LOG_TAG, "JSON Parsing finished");

                            if (response != null && parsedResponse.getWeatherForecast().length != 0) {
                                Log.d(LOG_TAG, "JSON not null and has " + parsedResponse.getWeatherForecast().length
                                        + " values");
                                Log.d(LOG_TAG, String.format("First value is %1.0f and %1.0f",
                                        parsedResponse.getWeatherForecast()[0].getMin(),
                                        parsedResponse.getWeatherForecast()[0].getMax()));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volley error", error.getMessage());

                    }
                });
        queue.add(jsonObjectRequest);
    }
}
